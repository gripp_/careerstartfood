const go = () => {
  const app = Vue.createApp({
    created() {
      setInterval(this.fetchInfo, 1500);
    },
    data() {
      return {
        name: undefined,
        order: undefined,
        orderStatus: undefined,
      };
    },
    methods: {
      fetchData(method, data) {
        return fetch(
            `/rpc/${method}`,
            {
              dataType: 'json',
              method: 'POST',
              headers: { 'Content-Type': 'application/json' },
              body: JSON.stringify(data),
            }).then(r => r.json());
      },
      fetchInfo() {
        if (this.name == undefined) {
          return;
        }

        this.fetchData(
            'info',
            { 'name': this.name, }).then(this.populate).catch(this.logError);
      },
      logError(err) {
        console.log(err);
      },
      placeOrder(order) {
        this.fetchData(
            'place',
            { 'name': this.name, 'order': order, }).then(
                this.fetchInfo).catch(this.logError);
      },
      populate(r) {
        this.name = r.name;
        this.order = r.order;
        this.orderStatus = r.orderStatus;
      },
      updateName(name) {
        if (name == undefined || name.length == 0) {
          return;
        }

        this.fetchData(
            'info',
            { 'name': name, }).then(this.populate).catch(this.logError);
      },
    },
  });

  app.component(
      'login-panel',
      {
        data() {
          return {
            name: '',
          };
        },
        template: `
          <div id="login">
            <input type="text" placeholder="name..." v-model="name" />
            <button @click="$emit('nameChanged', name)">login</button>
          </div>`
      });

  app.component(
      'place-order',
      {
        data() {
          return {
            order: '',
          };
        },
        template: `
          <div id="login">
            <input
                type="text" placeholder="enter your order..." v-model="order" />
            <button @click="$emit('orderSubmitted', order)">login</button>
          </div>`
      });

  app.component(
      'user-info',
      {
        props: [ 'name', 'order', 'orderStatus', ],
        template: `
          <div id="user-info">
            <h1>{{name}}</h1>
            <div v-if="order != undefined">
              <strong>Your Order: </strong>
              <span>{{order}}</span>
            </div>
            <div v-if="orderStatus != undefined">
              <strong>Order Status: </strong>
              <span>{{orderStatus}}</span>
            </div>
          </div>`
      });

  app.mount('#app');
};

document.addEventListener('DOMContentLoaded', go);
