import got from 'got';

let _orders = [];
const _pollFreq = 1000; // ms

// STATUSES
const NOT_STARTED = 0;
const IN_PROGRESS = 1;
const COMPLETE = 2;

const statusStrings = [
  'not started',
  'in progress...',
  'complete!',
];

export const getOrderStatus = async (name) => {
  const order = _orders.find(o => o.name === name);

  if (order === undefined) {
    return { order: undefined, orderStatus: undefined, };
  }

  return {
    order: order.order,
    orderStatus: statusStrings[order.status],
  };
};

export const placeOrder = async (name, order) => {
  const orderIndex = _orders.findIndex(o => o.name === name);

  if (orderIndex < 0) {
    _orders.push(
        {
          'name': name,
          'order': order,
          'status': NOT_STARTED,
        });
    return true;
  }

  if (_orders[orderIndex].status === NOT_STARTED) {
    _orders[orderIndex].order = order;
    return true;
  }

  return false;
};

const _pollChef = async () => {
  if (_orders.filter(o => o.status !== COMPLETE).length <= 0) {
    setTimeout(_pollChef, _pollFreq);
    return;
  }

  const currentOrder = _orders[0];
  let resp;

  try {
    resp = await got(
        'http://ec2-3-94-184-13.compute-1.amazonaws.com:8089/checkorder');
  } catch (error) {
    const burntOrder = _orders.shift();
    burntOrder.status = NOT_STARTED;
    _orders.push(burntOrder);
    _sortOrders();
    return;
  }

  const orderStatus = (
      JSON.parse(resp.body).status === 'ORDER_READY'
      ? COMPLETE
      : IN_PROGRESS);

  currentOrder.status = (
      currentOrder.status === NOT_STARTED || orderStatus === COMPLETE
      ? orderStatus
      : currentOrder.status);

  if (currentOrder.status === COMPLETE) {
    _sortOrders();
  }

  setTimeout(_pollChef, _pollFreq);
};

const _sortOrders = () => {
  const done = _orders.filter(o => o.status === COMPLETE);
  const notDone = _orders.filter(o => o.status != COMPLETE);

  _orders = notDone.concat(done);
};

_pollChef();
