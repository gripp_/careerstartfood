import { getOrderStatus, placeOrder, } from './chef.js';

const infoRpc = async (req, resp) => {
  const { order, orderStatus }  = await getOrderStatus(req.body.name);
  return {
    name: req.body.name,
    order: order,
    orderStatus: orderStatus,
  };
};

const placeRpc = async (req, resp) => {
  if (!placeOrder(req.body.name, req.body.order)) {
    resp.status(400);
  }

  return {};
};

export const paths = [
  { name: 'info', perform: infoRpc, },
  { name: 'place', perform: placeRpc, },
];
