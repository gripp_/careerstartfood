import express from 'express';
import fs from 'fs';

import { paths } from './rpc.js';

const app = express();

app.use(express.json());
app.use(express.static('fe'));

app.get('/', function (req, resp) {
  resp.redirect('/index.html');
});

app.post('/rpc/:path', async function(req, resp) {
  let content = null;
  let rpc = paths.find(p => p.name == req.params.path);

  if (!rpc) {
    resp.status(404);
    resp.send();
    return;
  }

  resp.send(JSON.stringify((await rpc.perform(req, resp))));
});

app.listen(8080);
